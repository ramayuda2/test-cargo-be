from .form import SignUpForm
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.urls import reverse


def AutoRedirect(request):
    return redirect(reverse('admin:MasterData_country_changelist'))

def register_user(request):
    msg = None
    success = False

    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            msg = 'User created - please <a href="/login">login</a>.'
            success = True

            # return redirect("/login/")

        else:
            msg = 'Form is not valid'
    else:
        form = SignUpForm()

    return render(request, "admin/register.html", {"form": form, "msg": msg, "success": success})
