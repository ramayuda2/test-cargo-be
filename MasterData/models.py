from collections.abc import Iterable
from django.db import models
from django_countries.fields import CountryField

# Create your models here.

class Country(models.Model):
    name = CountryField()
    flag = models.TextField(max_length=255, default="")
    currency = models.CharField(default="")

    def __str__(self):
        return "%s" % (self.name.name)

    def save(self, *args, **kwargs):
        self.flag = "/static/icon/%s.svg" % self.name.code.lower()
        self.currency = self.name.code
        return super(Country, self).save(*args, **kwargs)

    def get_name(self):
        return self.name.name

    class Meta:
        verbose_name = ('country')
        verbose_name_plural = ('country')

    def as_dict(self):
        return {
            'name': self.name.name,
            'flag': self.flag,
            'currency': self.currency
        }
    
class Category(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    price_per_kilo = models.IntegerField(default=0)

    def __str__(self):
        return "%s" % (self.title)
    
    class Meta:
        verbose_name = ('Category')
        verbose_name_plural = ('Category')