from django.contrib import admin
from MasterData.models import Category, Country
from MasterData.admin_views import CountryAdminView, CategoryAdminView
# Register your models here.


admin.site.register(Country, CountryAdminView)
admin.site.register(Category, CategoryAdminView)