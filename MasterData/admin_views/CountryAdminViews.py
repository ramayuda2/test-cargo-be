from django.contrib import admin


class CountryAdminView(admin.ModelAdmin):
    list_display = ('name', 'flag', 'currency')
    
    fieldsets = [
        (
            'General', {
                'fields': (
                    'name',
                )
            }
        ),
    ]

    def save_model(self, request, obj, form, change):
        qs = super().save_model(request, obj, form, change)
        obj.flag
        return 