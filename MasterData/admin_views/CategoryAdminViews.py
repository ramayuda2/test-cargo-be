from typing import Any
from django.contrib import admin


class CategoryAdminView(admin.ModelAdmin):
    list_display = ('country', 'title', 'price_per_kilo')
    
    fieldsets = [
        (
            'General', {
                'fields': (
                    'country',
                    'title',
                    'price_per_kilo'
                )
            }
        ),
    ]

    