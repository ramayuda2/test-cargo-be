from rest_framework import viewsets
from rest_framework_simplejwt.authentication import JWTAuthentication
from api.serializers import CategorySerializer, CountrySerializer, RegisterSerializer
from MasterData.models import Country, Category
from rest_framework import filters, response, exceptions, status
from rest_framework.decorators import authentication_classes, api_view, permission_classes
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated

import requests
import os
import json
# Create your views here.


class CountryViewset(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

class CategoryViewset(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['country_id']
    search_fields = ['title']

class RegisterViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer
    permission_classes = []
    authentication_classes = []

@api_view(['GET'])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def provinci(request):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3",  # noqa: E501
        "Content-Type": "application/json",
        "key": os.getenv('API_KEY')
    }
    req = requests.get(os.getenv('URL_API_PROVINCI')+ '?', headers=headers)
    if req.status_code == 200:
        return response.Response(req.json())
    else:
        return req.text

@api_view(['GET'])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def city(request):
    id = request.GET.get('province')
    
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3",  # noqa: E501
        "Content-Type": "application/json",
        "key": os.getenv('API_KEY')
    }

    req = requests.get(os.getenv('URL_API_CITY')+ '?province=%s' % id, headers=headers)
    if req.status_code == 200:
        return response.Response(req.json())
    else:
        return req.text
    
@api_view(['GET'])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def destination(request):
    
    search = request.GET.get('search')
    
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3",  # noqa: E501
        "Content-Type": "application/json",
        "key": os.getenv('API_KEY')
    }

    req = requests.get(os.getenv('URL_API_CITY')+ '?province=%s' % search, headers=headers)
    if req.status_code == 200:
        return response.Response(req.json())
    else:
        return req.text
    
@api_view(['POST'])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def calculation(request):

    body = json.loads(request.body)

    country_id = body.get('country_id')
    category_id = body.get('category_id')
    destination_id = body.get('destination_id')
    weight = body.get('weight')

    if not country_id:
        raise exceptions.APIException('Failed to get country', code=status.HTTP_400_BAD_REQUEST)
    if not category_id:
        raise exceptions.APIException('Failed to get category', code=status.HTTP_400_BAD_REQUEST)
    if not destination_id:
        raise exceptions.APIException('Failed to get destinasion', code=status.HTTP_400_BAD_REQUEST)
    if not weight:
        raise exceptions.APIException('Failed to get weight', code=status.HTTP_400_BAD_REQUEST)
    
    contry = Country.objects.get(id=country_id)
    category= Category.objects.get(id=category_id)

    international_price = weight * category.price_per_kilo 
    domestic_price = 0

    total_price = international_price + domestic_price

    destination = ""
    for i in destination_id:
        destination += i + " "

    context = {
        'origin': {
            'name':contry.name.name,
            'flag':contry.flag
        },
        'destination': destination,
        'category_name': category.title,
        'international_price': international_price,
        'domestic_price': domestic_price,
        'total_price': total_price
    }

    return response.Response(context, status=status.HTTP_200_OK)



    