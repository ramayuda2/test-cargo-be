from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView
)
from rest_framework import routers
from .views import CountryViewset, CategoryViewset, RegisterViewset, provinci, city, calculation, destination

Router = routers.DefaultRouter()
Router.register(r'country', CountryViewset)
Router.register(r'category', CategoryViewset)
Router.register(r'register', RegisterViewset)

urlpatterns = [
    path('login/', TokenObtainPairView.as_view(), name='login_api'),
    path('provinci/', provinci, name='provinci_api'),
    path('city/', city, name='city_api'),
    path('calculation/', calculation, name='calculation_api'),
    path('destination/', destination, name='destination_api'),
    path('', include(Router.urls))
]
